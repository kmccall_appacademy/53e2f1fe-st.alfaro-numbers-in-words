require 'byebug'
class Fixnum

  def in_words
    num_queue = create_num_queue
    in_word_value = []
    until num_queue.empty?
      digits = num_queue.shift
      word = consutrct_word_section(digits, num_queue.length)
      in_word_value << word unless word.empty?
    end
    in_word_value.join(" ")
  end

  def consutrct_word_section(digits, position)
    ##remove trailing 0
    clean_digits = clean_digits(digits)
    return [] if clean_digits.empty?

    in_word_value = [consutrct_word(clean_digits(digits), [])]
    position_value = append_position_value(position)
    in_word_value << position_value unless position_value.empty?
    in_word_value.join(" ")
  end

  def append_position_value(position)
    case position
    when 4 then "trillion"
    when 3 then "billion"
    when 2 then "million"
    when 1 then "thousand"
    else
      ""
    end
  end

  def consutrct_word(digits, base)
    return base.join(" ") if digits.empty? || digits.nil?
    digits_left = ""
    if digits.length == 3
      base << "#{conversion_words[digits[0]]} hundred"
      digits_left = clean_digits(digits[1..-1])
    elsif digits.length == 2
      if conversion_words[digits]
        base << conversion_words[digits]
      else
        temp_val = []
        temp_val << conversion_words[digits[0] + "0"]
        temp_val << conversion_words[digits[1]]
        base << temp_val.join(" ")
      end
    else
      base << conversion_words[digits[0]]
    end
    consutrct_word(digits_left, base)
  end

  def clean_digits(digits)
    ##remove trailing 0
    skip_digits = ["00", "000"]
    return "" if skip_digits.include?(digits)
    digits.to_i.to_s
  end

  ##I Hate this, however im not so sure how to make this hash a instance
  ##var that way i dont have to recreate this hash every time, which is
  ##awful, I could  pass this hash down the methods but that's patchy too
  def conversion_words
    {
      "0" => "zero",
      "1" => "one",
      "2" => "two",
      "3" => "three",
      "4" => "four",
      "5" => "five",
      "6" => "six",
      "7" => "seven",
      "8" => "eight",
      "9" => "nine",

      "10" => "ten",
      "11" => "eleven",
      "12" => "twelve",
      "13" => "thirteen",
      "14" => "fourteen",
      "15" => "fifteen",
      "16" => "sixteen",
      "17" => "seventeen",
      "18" => "eighteen",
      "19" => "nineteen",

      "00" => "",
      "20" => "twenty",
      "30" => "thirty",
      "40" => "forty",
      "50" => "fifty",
      "60" => "sixty",
      "70" => "seventy",
      "80" => "eighty",
      "90" => "ninety"
    }
  end

  def create_num_queue
    self.to_s.reverse.scan(/.{1,3}/).reverse.map(&:reverse)
  end
end
